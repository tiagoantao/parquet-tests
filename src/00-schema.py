'''Schema'''

import pyarrow as pa
import pyarrow.parquet as pq

simple_fields = [
    pa.field('POS', pa.uint32()),
    pa.field('desc', pa.string())
]


simple_schema = pa.schema(simple_fields)

simple_from_array = [pa.array([1]), pa.array(['bla'])]
simple_table = pa.Table.from_arrays(simple_from_array, ['POS', 'desc'])
print(simple_table)

with pq.ParquetWriter('simple.parquet', simple_schema,
                      version='2.0',
                      compression='snappy', flavor='spark') as w:
    w.write_table(simple_table)

#read_simple = pq.read_table('simple.parquet')
f = pq.ParquetFile('simple.parquet')
read_simple = f.read()


print(read_simple.to_pandas())
